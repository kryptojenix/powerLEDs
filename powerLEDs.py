#!/usr/bin/python3
import os
import time
import threading
import logging

import psutil
import subprocess
from subprocess import call

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from watchdog.events import LoggingEventHandler

# libraries to interact with the system
import psutil
from subprocess import call


# the config files
from powerLEDsConfig import *


logger = logging.getLogger("powerLEDs")

# logging.config = {
#     "filters" : {},
#     "formatters": {},
#     "handlers" : {},
#     "loggers" : {},
# }


import powerLEDsGPIO

# TODO: make folders relative to user home folder
# userName = os.getlogin()

# set path variables
# FIXME: detect installed path
installedPath = "/usr/sbin"

#FIXME: get the installed path
# or put in config file
installedName = 'powerLEDs'



class ServiceWatcher(threading.Thread):
    def __init__(self,serviceName,waitTime):
        threading.Thread.__init__(self)
        self.serviceName = serviceName + '.service'
        print(self.serviceName)
        self.waitTime = waitTime
        print("Watching %s every %d seconds" % (serviceName, waitTime))
        self.checkService()
    def checkService(self):
        with open (os.path.join(installedPath, "systemctl.txt"), 'r') as f:
            for serviceLine in f.read():
                if self.serviceName in serviceLine:
                    print(serviceLine)
                    print("%s is running" % self.serviceName)
                else:
                    print("ERROR, %s has stopped" % self.serviceName)
                    powerLEDsGPIO.error()
        time.sleep(self.waitTime)

class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        # light the red LED
        powerLEDsGPIO.error()

class MyFileHandler(FileSystemEventHandler):
    def on_modified(self, event):
        print("on_modified event: ", event)
        print(type(event))
        # note the type of event and take action
        if 'DirModifiedEvent' in str(type(event)):
            print("New logfile created")
            # LED goes Orange for 1 second
            powerLEDsGPIO.red(1)
            powerLEDsGPIO.green(1)
            powerLEDsGPIO.blue(0)
            # ORANGE for 1 second
            time.sleep(1)
            # back to default state
            #TODO: maintain the LEDstate (R,G,B)
            powerLEDsGPIO.red(0)
            powerLEDsGPIO.green(0)
            powerLEDsGPIO.blue(0)

        elif 'FileModifiedEvent' in str(type(event)):
            print("FileModifiedEvent")
            # LED goes red for 3 seconds
            powerLEDsGPIO.error()
            # RED for 3 seconds
            time.sleep(3)
            # back to default state
            #TODO: maintain the LEDstate (R,G,B)
            powerLEDsGPIO.good()

class ServiceWatcher(threading.Thread):
    def __init__(self,serviceName,waitTime):
        threading.Thread.__init__(self)
        self.serviceName = serviceName  # add extension + '.service'
        print(self.serviceName)
        self.waitTime = waitTime
        print("Watching %s every %d seconds" % (serviceName, waitTime))
        self.checkService()
    def checkService(self):
        p = subprocess.Popen(["ps", "-a"], stdout=subprocess.PIPE)
        out, err = p.communicate()
        if (self.serviceName in str(out)):
            print('%s running' % self.serviceName)
        else:
            print('%s stopped' % self.serviceName)
        time.sleep(self.waitTime)


def main():
    # logging.config.dictConfig(config=logging.config)

    fileWatchdog = False
    folderWatchdog = False
    serviceWatchdog = True
    processWatchdog = False

    # set up the media & log monitor paths
    # TODO: make this automatic to the users selection during install
    # or the app for which we are inidicating status
    # define the baseDir and load config
    baseDir = os.path.realpath(__file__).split(installedName)[0]+installedName

    # set up observers
    # TODO: be able to use multiple instances of each observer
    for service in services:
        print("Observing service : ", services[0])

    for process in processes:
        print("Observing process : ", processes[0])

    for folder in folders:
        print("Observing folder : ", folders[0])
    for log in logs:
        print("Observing log    : ", logs[0])

    print("Starting Watchdogs")
    event_handler = MyFileHandler()

    if folderWatchdog:
        #folderObserverPath = os.path.join('/home',userName, folders[0] )
        # use full path in config
        folderObserverPath = folders[0]
        print("Starting Folder Observer for %s" % folderObserverPath)
        folderObserver = Observer()
        folderObserver.schedule(event_handler, path=folderObserverPath, recursive=False)
        folderObserver.start()
        folderObserver.join()
    else:
        print("Not starting Folder Observer")


    if serviceWatchdog:
        print("starting watchdog for service: %s" % services[0])
        serviceWatchdog = ServiceWatcher(services[0], 5)
        serviceWatchdog.start()
        serviceWatchdog.join()
    else:
        print("Not starting Service Watchdog")


    powerLEDsGPIO.good()


    while True:
        # this section - monitor a process
        #try:
            #for pid in psutil.pids():
                #p = psutil.Process(pid)
                #if p.name() == services[0]:
                    #pass
                    ##print(p)
        #except KeyboardInterrupt:
            #pass
        #except:
            #if GPIOexists:
                #GPIO.output(17,GPIO.HIGH)         #red
                #GPIO.output(27,GPIO.LOW)        #green
                #GPIO.output(22,GPIO.LOW)         #blue
            ##call('/usr/local/sbin/notrack', shell=False)
            
        #finally:
            #print("done.")
            #time.sleep(1)
            
        

        #try:
            #for pid in psutil.pids():
                #p = psutil.Process(pid)
                #if p.name() == "notrack":
                    #pass
                    ##print(p)
        ##except KeyboardInterrupt:
            ##pass
        #except:
            #GPIO.output(17,GPIO.HIGH)         #red
            #GPIO.output(27,GPIO.LOW)        #green
            #GPIO.output(22,GPIO.LOW)         #blue
            ##call('/usr/local/sbin/notrack', shell=False)
            
        #finally:
            #time.sleep(1)

        if powerLEDsGPIO.pwr():
            # green LED off, blue LED on
            powerLEDsGPIO.green(0)
            powerLEDsGPIO.blue(1)
            call(['shutdown', 'now'], shell=False)
            # blue LED off when shutdown is complete...
            powerLEDsGPIO.blue(0)

       

if __name__ == "__main__":
    main()
else:
    print("__name__")

