#!/usr/bin/python3

try:
    import RPi.GPIO as GPIO
    GPIOexists = True
except:
    GPIOexists = False


print("GPIO Exists? : %s" % GPIOexists)


# TODO: get these from config
redPin = 27
greenPin = 22
bluePin = 23
# GPIO3 is the power on by default, so we use it for power off as well
# BUT..... GPI03 is used for I2C (SDA) so use something else if you need I2C
# or if you don't want to use the built-in power button of the RPi
powerPin = 16

if GPIOexists:
    # Set up the RPi GPIO pins to make the LEDs work
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(redPin,GPIO.OUT)
    GPIO.setup(greenPin,GPIO.OUT)
    GPIO.setup(bluePin,GPIO.OUT)
    GPIO.setup(powerPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

else:
    print("Starting without GPIO")


def main():
    print("This file should not be run directly")

def error():
    if GPIOexists:
        # light the red LED
        red(1)
        green(0)
        blue(0)
    else:
        print("Red")

def good():
    if GPIOexists:
        # light the green LED
        red(0)
        green(1)
        blue(0)
    else:
        print("Green")

def red(value):
    if GPIOexists:
        if value:
            GPIO.output(redPin,GPIO.HIGH)
        else:
            GPIO.output(redPin,GPIO.LOW)         
    else:
        print("RED=%s" % value)
    
def green(value):
    if GPIOexists:
        if value:
            GPIO.output(greenPin,GPIO.HIGH)
        else:
            GPIO.output(greenPin,GPIO.LOW)
    else:
        print("GREEN=%s" % value)

def blue(value):
    if GPIOexists:
        if value:
            GPIO.output(bluePin,GPIO.HIGH)
        else:
            GPIO.output(bluePin,GPIO.LOW)         
    else:
        print("BLUE=%s" % value)

def pwr():
    if GPIOexists:
        try:
            # GPIO.wait_for_edge(powerPin, GPIO.RISING)
            # the spare pin on the X820 SATA kit is normally closed, use FALLING edge
            GPIO.wait_for_edge(powerPin, GPIO.FALLING)
            return(True)
        except:
            return(False)

if __name__ == "__main__":
    main()
else:
    print("importing ", __name__)
