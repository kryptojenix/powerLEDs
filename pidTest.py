#!/usr/bin/python3
import time
import psutil
from subprocess import call
# Import the watchdog observers & events
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from watchdog.events import LoggingEventHandler

mediaPath = "./"
logPath = "./"

class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        print("file has changed")



event_handler = MyHandler()
observer = Observer()
observer.schedule(event_handler, path=mediaPath, recursive=False)
observer.start()


while True:
    try:
        for pid in psutil.pids():
            p = psutil.Process(pid)
            if p.name() == "firefox":
                print(p)
    except KeyboardInterrupt:
        pass
    except:   
        print("not running")
        call('/usr/bin/firefox', shell=False)
    finally:
        time.sleep(1)
