#!/bin/bash


# Copy the main python script and imports
sudo cp ./powerLEDs.py /usr/sbin/
sudo cp ./powerLEDsGPIO.py /usr/sbin
sudo cp ./powerLEDsConfig.py /usr/sbin
sudo cp ./LEDoff.py /usr/sbin


# set file permissions
sudo chmod 744 /usr/sbin/powerLEDs.py
sudo chmod 744 /usr/sbin/powerLEDsGPIO.py
sudo chmod 744 /usr/sbin/powerLEDsConfig.py
sudo chmod 744 /usr/sbin/LEDoff.py


# Copy the system service files
#sudo cp ./ledpowerswitch.conf /ffff/
sudo cp ./ledpowerswitch.service /etc/systemd/system/
sudo chmod 644 /etc/systemd/system/ledpowerswitch.service


# Create a file to hold the list of running services
sudo touch /usr/sbin/systemctl.txt
sudo chmod 644 /usr/sbin/systemctl.txt


# create log and media directories for the watchdog
# TODO: put this in the python script to be done at first run
#mkdir ~/media
#mkdir ~/logs


# TODO: ask to update
# sudo apt update

# TODO: (for testing) if gpio exists (on a RPi)
# sudo apt install python3-rpi.gpio python3-watchdog python3-psutil

sudo /usr/sbin/LEDoff.py
sudo systemctl stop ledpowerswitch
sudo systemctl daemon-reload
sudo systemctl start ledpowerswitch
# TODO: if success
sudo systemctl enable ledpowerswitch



