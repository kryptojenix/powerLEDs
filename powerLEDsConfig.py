
folders = [
    'media',
    ]

logs = [
    '/var/log/snort/'
    ]
processes = [
    'motion'
    ]

services = [
    'jellyfin.service'
    ]

if __name__ == "__main__":
    print("This file should not be run directly, use powerLEDs.py")
else:
    print("importing ", __name__)
