#!/usr/bin/python

# gpio disabled for testing
#import RPi.GPIO as GPIO
#GPIO.setmode(GPIO.BCM)
#GPIO.setwarnings(False)
#GPIO.setup(17,GPIO.OUT)
#GPIO.setup(27,GPIO.OUT)
#GPIO.setup(22,GPIO.OUT)

import sys


data = sys.argv
print("Vars from shell: ", len(data))
for x in data:
    print(x)



LEDduration = 0
LEDindex = [1,1,1]

def sanity(data):
    #
    #if isinstance(data[1], bool)
    Rvalue = bool(data[1])
    print(type(Rvalue))
    
    Gvalue = bool(data[2])
    print(type(Gvalue))
    
    Bvalue = bool(data[3])
    print(type(Bvalue))
    
    LEDduration = int(data[4])
    print(type(LEDduration))
    
    return(Rvalue, Gvalue, Bvalue, LEDduration)
        
    

def main():
    print("Starting LEDon.py main function")
    userInput = True
    if len(data) == 0:
        print("all on")
        userInput=False
        all_on()

    elif len(data) == 5:
        #sanity check
        Rvalue, Gvalue, Bvalue, LEDduration = sanity(data)
        
      
        # end of sanity check
        
        LEDindex = [Rvalue, Gvalue, Bvalue]
    
        for LEDvalue in LEDindex:
            print(LEDvalue)
            
    else:
        print("failed first sanity check")
        userInput = False
        #break
    
    # only continue if the sanity check passed
    if userInput:
        print("apply settings")
        turn_on(LEDindex)
        
    else:
        print("no valid options. quitting")
    
    
    
   


def turn_on(LEDindex):
    
    for i, LEDstate in enumerate(LEDindex):
        print(i, "state is : ",LEDstate)
    #print("Which LED? :", LEDcolor)
    #print("On?        :", LEDvalue)
    #print("Duration   :", LEDduration)

    
    
    
    
    
def all_on():
    print("all LEDs on")
    #GPIO.output(17,GPIO.HIGH)
    #GPIO.output(27,GPIO.HIGH)
    #GPIO.output(22,GPIO.HIGH)
    #GPIO.cleanup()



if __name__ == "__main__":
    main()
